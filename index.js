let users = ["Dwayne Johnson","Steve Austin","Kurt Angle","Dave Bautista"];

console.log("Original Array:")
console.log(users);


function firstArgument(starPerson){
        users = ["Dwayne Johnson","Steve Austin","Kurt Angle","Dave Bautista"];
        users[users.length] = starPerson;
        console.log(users);
        return users;
}
firstArgument("John Cena")


function secondArgument(users){
        index = users[2];
        console.log(index);
}
secondArgument(users)

function thirdArgument(removeStar){
        index = users[4];
        console.log(index);
        // users = ["Dwayne Johnson","Steve Austin","Kurt Angle","Dave Bautista"];
        removeStar[removeStar.length--];
        console.log(removeStar);
        return thirdArgument;
}
thirdArgument(users)

function fourthArgument(arrayOfNames){
        arrayOfNames = users;
        // arrayOfNames.splice(3,3);
        arrayOfNames[3] = 'Triple H';
        console.log(arrayOfNames);
        return arrayOfNames;
}
fourthArgument(users)

function fiftArgument(arrayOfNames){
        arrayOfNames = users;
        arrayOfNames[arrayOfNames.length--];
        arrayOfNames[arrayOfNames.length--];
        arrayOfNames[arrayOfNames.length--];
        arrayOfNames[arrayOfNames.length--];
        console.log(arrayOfNames);
        return arrayOfNames;
}
fiftArgument(users)

function sixthArgument(arrayOfNames){

    let arrayTrue = true;
    let arrayFalse = false;

    if (arrayOfNames.length > 0){
        console.log(arrayFalse);
    }
    else{
        console.log(arrayTrue);
    }
}
sixthArgument(users)
